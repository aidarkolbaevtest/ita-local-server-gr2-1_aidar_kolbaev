<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, length=254)
     */
    private $email;


    /**
     * @ORM\Column(type="string", length=254)
     */
    private $password;


    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $roles;


    /**
     * @var string
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    private $passport;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    public function __construct()
    {
        $this->registeredAt = new \DateTime('now', new \DateTimeZone('Asia/Bishkek'));
        $this->roles = json_encode(['ROLE_USER']);
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }


    public function addRole(string $role)
    {
        $roles = $this->getRoles();
        $roles[] = $role;
        $this->setRole($roles);
    }


    public function setRole(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * @param $username
     * @return User
     */
    public function setUsername($username)
    {
        return $this->setEmail($username);
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }

    public function getRegisteredAtToString() {
        return date_format($this->registeredAt, 'Y-m-d H:i:s');
    }

    /**
     * @param string $passport
     * @return User
     */
    public function setPassport(string $passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param string $registeredAt
     * @return User
     */
    public function setRegisteredAt(string $registeredAt): User
    {
        $this->registeredAt = new \DateTime($registeredAt, new \DateTimeZone('Asia/Bishkek'));
        return $this;
    }
}
