<?php

namespace App\Model\User;


use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     * @param bool $encode
     * @return User
     */
    public function createUser(array $data, bool $encode = true) {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        if (isset($data['registeredAt'])) {
            $user->setRegisteredAt($data['registeredAt']);
        }
        if ($encode) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }
        $user->setPassword($password);
        return $user;
    }

    public function encodePlainPassword(string $password):string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));
    }
}