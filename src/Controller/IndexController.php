<?php

namespace App\Controller;


use App\Entity\User;
use App\Form\LoginType;
use App\Form\RegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function indexAction(ApiContext $apiContext)
    {
        try {
            $apiContext->makePing();
            return $this->render('index.html.twig', [
               //
            ]);
        } catch (ApiException $e) {
            return new Response($e->getMessage());
        }
    }


    /**
     * @Route("/register", name="sign_up")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @return Response
     */
    public function registerAction(Request $request, ApiContext $apiContext, UserHandler $userHandler)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $result = $apiContext->doesClientExist($user->getEmail(), $user->getPassport());
            } catch (ApiException $e) {
                return new Response('Checking error: ' . $e->getMessage());
            }
            if ($result) {
                $error = true;
            } else {
                try {
                    $data = [
                        'email' => $user->getEmail(),
                        'password' => $user->getPassword(),
                        'passport' => $user->getPassport()
                    ];
                    $apiContext->registerClient($data);
                } catch (ApiException $e) {
                    return new Response('Registration error: ' . $e->getMessage());
                }
                $user = $userHandler->createUser($data, false);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('homepage');
            };
        }

        return $this->render('registration.html.twig', array(
            'form' => $form->createView(),
            'error' => isset($error),
        ));
    }


    /**
     * @Route("/login", name="sign_in")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function authAction(Request $request, ApiContext $apiContext, UserRepository $userRepository, UserHandler $userHandler)
    {
        $form = $this->createForm(LoginType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->findOneByPasswordAndEmail($data['password'], $data['email']);
            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('homepage');
            } else {
                try {
                    $result = $apiContext->doesClientExist($data['email'], null, $data['password']);
                } catch (ApiException $e) {
                    return new Response('Checking error: ' . $e->getMessage());
                }
                if (!$result) {
                    $error = true;
                } else {
                    $user = $userHandler->createUser((array) $result, false);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('homepage');
                }
            }
        }
        return $this->render('sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => isset($error)
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        $this->get('security.token_storage')->setToken(null);
        $this->container->get('session')->invalidate();
        return $this->redirectToRoute('homepage');
    }
}
